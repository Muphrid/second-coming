# Introduction

*The Second Coming* is a fan novel for *Neon Genesis Evangelion*.  This repository holds the source files, storyboards, and build script for generating files in bbcode and fanfiction.net-compatible html.

# About the repository

The repistory has three main directories:

* the top level directory, which holds `.tex` files.  These are the main story files.  The readme and an additional file with a draft of summaries are in here.
* the `brd` directory, with storyboards and notes
* the `build` directory, which has the main perl script for generating FFN and bbcode files

# About the story

*The Second Coming* takes places two years after the end of *Neon Genesis Evangelion*.  Shinji Ikari is a public figure who has shied away from the spotlight, but when Seele make an attempt on his life, he delves into the growing plot to force humanity back to Instrumentality and dismantle civilization.  Shinji trains the new pilot of Eva Unit-14--Nozomi Horaki, sister of Hikari--to give all mankind the power to decide if they wish to exist.

But Seele are not the only ones who wish to see humanity return to the see.  From the depths of space, where the new Angels came, the sister of Adam and Lilith looks on, and she sends her children and Angels alike to force men back to nothingness.

# Generating output files

The `build` directory holds the `strip-tex` script.  This is a hodgepodge and not something recommended for general use.  The basic syntax is:

`./strip-tex (output option) (input file) > (output file)`

where `output option` is

* `-f` for fanfiction.net style html
* `-b` for bbcode

Some niceties are not done automatically: chapter numbering and subtitles, for instance, are not done for you.  Still, the output file should be pretty close to what you want.
